/****************************************************************************
*
*	Copyright (C) 2001 Steffi Gebauer-Jung
*				  Max-Planck-Institute of Chemical Ecology
*				  Jena, Germany															
*                 All rights reserved.
*
* Language:     ANSI C
*
* Description:	
*
* $Id: ali2gff.h,v 1.4 2001/09/26 16:01:50 steffi Exp $
*
****************************************************************************/

/*****************************************************************************
* include                                                                    * 
*****************************************************************************/

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>

/*****************************************************************************
* definitions                                                                    * 
*****************************************************************************/
#define MAXSTRING 1000
#define MAX_SEQUENCE_LENGTH 10001000
#define MAXARRAY 410000
#define ALLOC 1000

/*****************************************************************************
* typedef                                                                    * 
*****************************************************************************/
static struct {
	long begin_clock;
	long begin_time;
} timer = {0,0}; 

/*****************************************************************************
* function headers                                                           * 
*****************************************************************************/
void doit();
void processMumfile();
void processGFFfile();
void start_time(void);
void print_time(void);
void printUsage(void);
void wrongFormatError(const char* filename);
void allocationError();

int sortScores(const float *score1, const float *score2);
void processSimfile();
